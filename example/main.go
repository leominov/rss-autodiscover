package main

import (
	"fmt"

	"github.com/leominov/rss-autodiscover"
)

var (
	TestData = []string{
		"https://blog.twitch.tv/tagged/engineering",
		"https://medium.com/nbc-news-digital",
		"https://hackernoon.com",
		"https://splice.com/blog/category/engineering/",
		"https://blog.shazam.com/tagged/engineering",
		"http://codevoyagers.com/",
		"https://medium.com/wizeline-engineering",
		"https://blog.heroku.com/engineering",
		"https://stripe.com/blog/engineering",
		"https://medium.com/jobteaser-dev-team/",
		"http://blog.mallow-tech.com",
		"http://facebook.github.io/react-native/blog/",
		"https://medium.com/myntra-engineering",
		"https://airtame.engineering",
		"https://deezer.io",
		"http://engineering.curalate.com/",
		"https://www.hostinger.com/blog/engineering/",
		"http://engineering.hoteltonight.com",
		"http://product.hubspot.com/blog/topic/engineering",
		"http://engineering.criteolabs.com/",
		"https://blog.sqreen.io",
		"https://www.novoda.com/blog/",
		"https://medium.com/zendesk-engineering",
		"https://tech.zalando.com/blog/",
		"http://blog.bitrise.io",
		"https://www.cockroachlabs.com/blog/",
		"https://www.appveyor.com/blog/",
		"http://circleci.com/blog/",
		"http://engineering.opensooq.com",
		"https://benmccormick.org/",
		"http://devopscafe.org",
		"https://appfolio-engineering.squarespace.com",
		"http://www.lyh.me",
		"http://digest.deeplearningweekly.com",
		"https://wit.ai/blog",
		"https://api.ai/blog/",
		"https://code.facebook.com",
		"https://blog.converse.ai",
	}
)

func main() {
	for _, url := range TestData {
		feedUrl, err := autodiscover.Find(url)
		if err != nil {
			fmt.Printf("%s error: %v\n", url, err)
			continue
		}
		fmt.Println(url, feedUrl)
	}
}
