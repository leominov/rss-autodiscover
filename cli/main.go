package main

import (
	"fmt"
	"os"

	autodiscover "github.com/leominov/rss-autodiscover"
)

func main() {
	for _, url := range os.Args[1:] {
		feedUrl, err := autodiscover.Find(url)
		if err != nil {
			fmt.Printf("%s: %v\n", url, feedUrl)
			continue
		}
		fmt.Printf("%s: %s\n", url, feedUrl)
	}
}
