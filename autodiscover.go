package autodiscover

import (
	"bytes"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"net/url"
	"strings"

	"golang.org/x/net/html"
	"golang.org/x/net/html/atom"
)

var (
	feedContentTypes = map[string]bool{
		"application/x.atom+xml": true,
		"application/atom+xml":   true,
		"application/xml":        true,
		"text/xml":               true,
		"application/rss+xml":    true,
		"application/rdf+xml":    true,
	}
	feedUrlPostfix = []string{
		"rss",
		"feed",
		"atom",
		"rss.xml",
		"feed.xml",
		"atom.xml",
		".rss",
		".atom",
		"/rss.xml",
		"/feed.xml",
		"/atom.xml",
	}
)

var ErrNoRssLink = errors.New("No rss link found")

func hasContentTypeHeader(url string) (bool, error) {
	resp, err := http.Head(url)
	if err != nil {
		return false, err
	}
	if resp.StatusCode != http.StatusOK {
		return false, fmt.Errorf("Incorrect response status code: %v", resp.Status)
	}
	ct := resp.Header.Get("Content-Type")
	headerParts := strings.Split(ct, ";")
	for _, part := range headerParts {
		v := strings.ToLower(strings.TrimSpace(part))
		if _, ok := feedContentTypes[v]; ok {
			return true, nil
		}
	}
	return false, nil
}

func autodiscoverByHTML(b []byte) (string, error) {
	r := bytes.NewReader(b)
	z := html.NewTokenizer(r)
	inHtml := false
	inHead := false
	for {
		if z.Next() == html.ErrorToken {
			if err := z.Err(); err == io.EOF {
				break
			} else {
				return "", ErrNoRssLink
			}
		}
		t := z.Token()
		switch t.DataAtom {
		case atom.Html:
			inHtml = !inHtml
		case atom.Head:
			inHead = !inHead
		case atom.Link:
			if inHead && inHtml && (t.Type == html.StartTagToken || t.Type == html.SelfClosingTagToken) {
				attrs := make(map[string]string)
				for _, a := range t.Attr {
					attrs[a.Key] = a.Val
				}
				if attrs["rel"] == "alternate" && attrs["href"] != "" {
					if _, ok := feedContentTypes[attrs["type"]]; ok {
						return attrs["href"], nil
					}
				}
			}
		}
	}
	return "", ErrNoRssLink
}

func autodiscoverByURL(urlFetch string) (string, error) {
	base, err := url.Parse(urlFetch)
	if err != nil {
		return "", err
	}
	for _, postfix := range feedUrlPostfix {
		u, err := url.Parse(postfix)
		if err != nil {
			continue
		}
		l := base.ResolveReference(u).String()
		if valid, _ := hasContentTypeHeader(l); valid {
			return l, nil
		}
	}
	return "", ErrNoRssLink
}

func Find(urlFetch string) (string, error) {
	u, err := url.Parse(urlFetch)
	if u.Host == "" {
		u.Host = u.Path
		u.Path = ""
	}
	if err == nil && u.Scheme == "" {
		u.Scheme = "http"
	}
	if resp, err := http.Get(urlFetch); err == nil && resp.StatusCode == http.StatusOK {
		defer resp.Body.Close()
		b, _ := ioutil.ReadAll(resp.Body)
		if autoUrl, err := autodiscoverByHTML(b); err == nil {
			if autoU, err := url.Parse(autoUrl); err == nil {
				if autoU.Scheme == "" {
					autoU.Scheme = u.Scheme
				}
				if autoU.Host == "" {
					autoU.Host = u.Host
				}
				autoUrl = autoU.String()
			}
			if valid, _ := hasContentTypeHeader(autoUrl); valid {
				return autoUrl, nil
			}
		}
	}
	if autoUrl, err := autodiscoverByURL(urlFetch); err == nil {
		return autoUrl, nil
	}
	return "", ErrNoRssLink
}
